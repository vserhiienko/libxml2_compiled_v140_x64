# LIBXML2 2.9.2 #

libxml2 2.9.2, compiled with Visual Studio 2015 (v140;x64;debug,release). 
Also, contains libiconv builds (same configurations). 
It was compiled as a part of OpenRAVE library, but due to large size it was moved to separate repo.

* vlad.serhiienko@gmail.com
* http://www.xmlsoft.org/